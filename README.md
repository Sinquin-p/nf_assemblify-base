#  nf_assemblify-base pipeline

This is a project associated to this [training material](https://bioinfo.gitlab-pages.ifremer.fr/teaching/fair-gaa/practical-case/). This project is a fork of this [gitlab repository](https://gitlab.com/ifb-elixirfr/training/workflows/nf_assemblify-base).


## Introduction

nf_assemblify-base is a [Nextflow](https://www.nextflow.io/)-based genome assembly pipeline. The objective of this pipeline is to assemble and annotate a genome from reads in fastq format. The pipeline has been tested with this [data set](https://data-dataref.ifremer.fr/bioinfo/ifremer/sebimer/UE-FAIR-Rennes/) on IFB Core Cluster.  

nf_assemblify-base pipeline is a modular Nextflow pipeline relying on two key features available from Nextflow system:  

* It relies on Modules; i.e. each step of the pipeline is isolated within a dedicated code available in <code>bin</code> and <code>module</code> sub-folders  
* It uses Singularity containers to run steps  

## nf_assemblify-base architecture

The pipeline contains the following skeleton:

* <code>conf</code> directory contains the configuration files of the workflow  
    * <code>conf/resources.config</code> file contains RAM/CPU/TIME specs to be used with tools  
* <code>bin</code> directory contains the shell scripts executing tools  
* <code>module</code> directory contains the Nexflow steps (aka processes) of the overall pipeline  
    * each module handles in/out channels, call of ‘bin’-located shell script, and publishing of results  
* <code>main.nf</code> file discribes the chaining of modules (the workflow itself)  
* <code>nextflow.config</code> file sets up the Singularity images to use during pipeline execution  
* <code>ifb-core.config</code> file contains the resources definition to execute the pipeline on IFB Core Cluster  
* <code>run_assemblify.slurm</code> file is a helper script to submit pipeline execution on a cluster  



## Pipeline overview

<div align="center">

  <img src="image/pipeline-gaa.png" height="500">
</div>

## Pipeline stages

### 1. Quality control

#### 	1.1 Nanoplot

[NanoPlot](https://github.com/wdecoster/NanoPlot) is a plotting tool for long read sequencing data and alignments.  
  
* **Input data:** PacBio sequencing files (.fastq.gz)  
* **Output data:** Folder with a web report (.html), a plot (.png) and statistics (.txt)  

#### 1.2 FastQC

[FastQC](https://github.com/s-andrews/FastQC) is a program designed to spot potential problems in high througput sequencing datasets.  

* **Input data:** Ilumina sequencing files (.fastq.gz)  
* **Output data:** A web report (.html) and a archive (.zip)    

#### 1.3 Multiqc

[MultiQC](https://github.com/MultiQC/MultiQC) is a tool to create a single report with interactive plots for multiple bioinformatics analyses across many samples. It allows to aggregate Illumina FastQC results.  

* **Input data:** Archive from FastQC (.zip)    
* **Output data:** A web report (.html)  


###  2. Contiging

#### 2.1 Hifiasm

[Hifiasm](https://github.com/chhylp123/hifiasm) is a fast haplotype-resolved de novo assembler initially designed for PacBio HiFi reads.  
  
* **Input data:** PacBio sequencing files (.fastq.gz)  
* **Output data:** Assembly sequences (.fasta) and assembly graph (.gfa)

#### 2.2 BUSCO

[BUSCO](https://busco.ezlab.org/) is a tool to assess genome assembly and annotation completeness with Benchmarking Universal Single-Copy Orthologs  
  
* **Input data:** Assembly sequences from Hifiasm (.fasta)  
* **Output data:** Folder with subfolder and statistics (.tsv)


###  3. Reapat masking

#### 3.1 RED

[RED](https://github.com/red/red) is used for genome assembly repeats softmasking.  

* **Input data:** Assembly sequences from Hifiasm (.fasta)  
* **Output data:** Folder with softmasked assembly sequences (.msk)


### 4. Evidences

#### 4.1 HISAT2 / Samtools

[HISAT2](https://github.com/DaehwanKimLab/hisat2) is a fast and sensitive alignment program for mapping next-generation sequencing reads (whole-genome, tanscriptome, and exome sequencing data) to a population of human genomes (as well as to a single reference genome). [Samtools](http://www.htslib.org/) is a suite of programs for interacting with high-throughput sequencing data.  

* **Input data:** Illumina sequencing files (.fastq.gz) and softmasked assembly sequences from RED (.msk)  
* **Output data:** Genome index (.ht2), mapping file (.bam) and mapping file index (.bai)  

**This step sadly does not work, therefore the following steps have not been implemented.** Because the hisat2 branch is still in development, the branch was not merge in our master branch.

#### 4.2 MultiQC

[MultiQC](https://github.com/MultiQC/MultiQC) is a tool to create a single report with interactive plots for multiple bioinformatics analyses across many samples. It allows to aggregate Hisat2 mapping results.  

* **Input data:** Log files from Hisat2 (.log)  
* **Output data:** A web report (.html)


### 5. Prediction

#### 5.1 BRAKER3

[BRAKER3](https://github.com/Gaius-Augustus/BRAKER) is a combination of GeneMark-ET and AUGUSTUS, that uses genomic and RNA-Seq data to automatically generate full gene structure annotations in novel genome.  

* **Input data:** Softmasked assembly sequences from RED (.msk), mapped reads from Hisa2t (.bam) and proteins from short evolutionary distance (.faa)  
* **Output data:** Folder with many files: Structural annotation (.gff3) and proteins sequences from annotation (.faa)  


## How to start the pipeline

Clone the current gitlab repository in your working directory on IFB Core Cluster:  
```bash
git clone https://gitlab.com/Sinquin-p/nf_assemblify-base.git
cd nf_assemblify-base
```

Change <code>ASSEMBLIFY_DATA_PATH</code> (run_assemblify.slurm line 24) with the path to your dataset:
```bash
export ASSEMBLIFY_DATA_PATH=the_path_to_my_dataset
```

Change <code>WK_DIR</code> (run_assemblify.slurm line 16) with the path to an assemblify-tmp directory in your working directory:
```bash
WK_DIR=the_path_to_my_working_directory/assemblify-tmp
```

Run the pipeline:  
```bash
sbatch run_assemblify.slurm
```

Monitor the job:  
```bash
squeue -u $USER
tail -f assemblify.log 
```

Find results directories of all steps in <code>results/assemblify/</code> (sub-folders specified in .nf files in <code>module</code>).  

## Authors

This pipeline is derived from a step-by-step analysis carried out on Ifremer’s computer cluster. It was developed by Pierre Sinquin, Zoé Le Roux and Juliette Francis, Master 2 Bioinformatics students at Rennes University, as part of the PLA course (FAIR principles applied to genome assembly and annotation).
