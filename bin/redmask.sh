#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##                              Repeat masking                               ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
DIR=${args[0]}
NCPUS=${args[1]}
LOGCMD=${args[2]}

# Command to execute
CMD="Red -gnm . -msk masked -cor $NCPUS"
echo $CMD > ${LOGCMD}

mkdir -p masked

eval ${CMD}
