#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##                  Quality control of reads using NanoPlot                  ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
fastq=${args[0]}
cpus=${args[1]}
LOGCMD=${args[2]}

# Command to execute
CMD="NanoPlot -t ${cpus} --plots kde dot hex --N50 --title 'PacBio Hifi reads for $(basename ${fastq%.hifi*})' --fastq ${fastq} -o ."

# Keep command in log
echo ${CMD} > ${LOGCMD}

# Execute command
eval ${CMD}

