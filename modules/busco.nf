process busco {

    label 'midmem'

    tag "busco"

    publishDir "${params.resultdir}/02b_busco",		mode: 'copy', pattern: '*.busco'
    publishDir "${params.resultdir}/logs/busco",	mode: 'copy', pattern: 'busco*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'busco*.cmd'

    input:
        path(budsco_db_path)
	    val(busco_db_name)
	    path(assembly_fa)

    output:
        path("*.busco")
        path("busco*.log")
        path("busco*.cmd")

    script:
    """
    busco.sh ${task.cpus} ${budsco_db_path} ${busco_db_name} ${assembly_fa} busco.cmd  >& busco.log 2>&1
    """ 
}



