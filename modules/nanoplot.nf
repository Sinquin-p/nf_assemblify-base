process nanoplot {

    label 'lowmem'

    tag "NanoPlot"

    publishDir "${params.resultdir}/01c_nanoplot",	mode: 'copy', pattern: '{*.html,*.png,*.txt}'
    publishDir "${params.resultdir}/logs/nanoplot",	mode: 'copy', pattern: 'nanoplot*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'nanoplot*.cmd'

    input:
        path(qc_ch)

    output:
        path("*.*")
        path("*.log")
        path("*.cmd")

    script:
    """
    nanoplot.sh $qc_ch ${task.cpus} nanoplot_${qc_ch.baseName}.cmd >& nanoplot_${qc_ch.baseName}.log 2>&1
    """ 
}



